/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _classworks_task2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../classworks/task2 */ \"./classworks/task2.js\");\n//import BeachParty from '../classworks/task1';\n//BeachParty()\n\nObject(_classworks_task2__WEBPACK_IMPORTED_MODULE_0__[\"default\"])(); //import Work1 from '../classworks/task3_decorators_es7';\n//Work1();\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./classworks/task2.js":
/*!*****************************!*\
  !*** ./classworks/task2.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/*\n  Повторить задание с оповещаниями (application/DecoratorExample), с\n  использованием нескольких уровней абстракций, а именно паттерны:\n  Decorator, Observer, Fabric\n\n  Задача: Написать динамичную систему оповещений, которая будет отправлять\n  сообщения все подписаным на неё \"Мессенджерам\".\n  Картинки мессенджеров есть в папке public/images\n\n  Класс оповещения должен иметь декоратор на каждый мессенджер.\n\n  При создании обьекта класса Application нужно передавать обьект\n  в котором будут находится те \"Мессенджеры\" который в результате будут\n  подписаны на этот блок приложения.\n\n  Отправка сообщения по \"мессенджерам\" должна происходить при помощи\n  паттерна Observer.\n\n  При отправке сообщения нужно создавать обьект соответствующего класса,\n  для каждого типа оповещания.\n\n  let header = new Application('slack', 'viber', 'telegramm');\n  let feedback = new Application('skype', 'messanger', 'mail', telegram);\n\n  btn.addEventListener('click', () => header.sendMessage(msg) );\n\n  Архитектура:\n  Application( messanges ) ->\n    notfier = new Notifier\n    renderInterface(){...}\n  Notifier ->\n    constructor() ->\n      Fabric-> Фабрикой перебираете все типы месенджеров которые\n      подписаны на эту Application;\n    send() -> Отправляет сообщение всем подписчикам\n\n*/\nclass Application {\n  constructor(messangers) {\n    this.messangers = messangers;\n    this.notifier = new BaseDecorator(messangers);\n    this.createInterface = this.createInterface.bind(this);\n  }\n\n  createInterface() {\n    const root = document.getElementById('root');\n    const AppNode = document.createElement('section');\n    AppNode.className = 'notifer_app';\n    AppNode.innerHTML = `\n      <div class=\"notifer_app--container\">\n        <header>\n          <input class=\"notifier__messanger\" type=\"text\"/>\n          <button class=\"notifier__send\">Send Message</button>\n        </header>\n        <div class=\"notifier__container\">\n        ${this.messangers.map(item => `\n            <div class=\"notifier__item\" data-slug=\"${item.name}\">\n              <header class=\"notifier__header\">\n                <img width=\"25\" src=\"${item.image}\"/>\n                <span>${item.name}</span>\n              </header>\n              <div class=\"notifier__messages\"></div>\n            </div>\n            `).join('')}\n        </div>\n      </div>\n    `;\n    const container = AppNode.querySelector('.notifier__container');\n    const btn = AppNode.querySelector('.notifier__send');\n    const input = AppNode.querySelector('.notifier__messanger');\n    btn.addEventListener('click', () => {\n      let value = input.value;\n      this.notifier.sendMessage(value, this.node);\n    });\n    this.node = AppNode;\n    root.appendChild(AppNode);\n  }\n\n}\n\nlet messangers = [{\n  name: 'sms',\n  image: '../public/images/sms.svg'\n}, {\n  name: 'mail',\n  image: '../public/images/gmail.svg'\n}, {\n  name: 'telegram',\n  image: '../public/images/telegram.svg'\n}, {\n  name: 'viber',\n  image: '../public/images/viber.svg'\n}, {\n  name: 'slack',\n  image: '../public/images/slack.svg'\n}];\n\nclass Notifier {\n  send(msg, baseNode, block) {\n    console.log('CLASS NOTIFIER: mesasge was sended:', msg);\n    const target = baseNode.querySelector(`.notifier__item[data-slug=\"${block}\"]`);\n    target.innerHTML += `<div>${msg}</div>`;\n  }\n\n}\n\nclass SmsNotifier extends Notifier {\n  send(msg, baseNode, block = 'sms') {\n    super.send(msg, baseNode, block);\n  }\n\n}\n\nclass ViberNotifier extends Notifier {\n  send(msg, baseNode, block = 'viber') {\n    super.send(msg, baseNode, block);\n  }\n\n}\n\nclass GmailNotifier extends Notifier {\n  send(msg, baseNode, block = 'mail') {\n    super.send(msg, baseNode, block);\n  }\n\n}\n\nclass TelegramNotifier extends Notifier {\n  send(msg, baseNode, block = 'telegram') {\n    super.send(msg, baseNode, block);\n  }\n\n}\n\nclass SlackNotifier extends Notifier {\n  send(msg, baseNode, block = 'slack') {\n    super.send(msg, baseNode, block);\n  }\n\n}\n\nclass BaseDecorator {\n  constructor(clients) {\n    let obs = clients.map(obs => {\n      if (obs.name === 'sms') {\n        return new SmsNotifier(obs);\n      } else if (obs.name === 'mail') {\n        return new GmailNotifier(obs);\n      } else if (obs.name === 'telegram') {\n        return new TelegramNotifier(obs);\n      } else if (obs.name === 'viber') {\n        return new ViberNotifier(obs);\n      } else if (obs.name === 'slack') {\n        return new SlackNotifier(obs);\n      }\n    });\n    this.clients = obs;\n  }\n\n  sendMessage(msg, baseNode) {\n    this.clients.map(obs => {\n      obs.send(msg, baseNode);\n    });\n  }\n\n}\n\nconst Demo = () => {\n  let header = new Application([{\n    name: 'mail',\n    image: '../public/images/gmail.svg'\n  }, {\n    name: 'telegram',\n    image: '../public/images/telegram.svg'\n  }, {\n    name: 'viber',\n    image: '../public/images/viber.svg'\n  }]);\n  let feedback = new Application([{\n    name: 'sms',\n    image: '../public/images/sms.svg'\n  }, {\n    name: 'slack',\n    image: '../public/images/slack.svg'\n  }]);\n  header.createInterface();\n  feedback.createInterface();\n};\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Demo);\n\n//# sourceURL=webpack:///./classworks/task2.js?");

/***/ })

/******/ });