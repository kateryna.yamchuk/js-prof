import Calc, {AddCommand, SubCommand, MultiplyCommand, DivideCommand} from './calculator'

const ExecCalc = new Calc(); 

test('Adding', () => {
  ExecCalc.execute( new AddCommand( 2 ) );
  ExecCalc.execute( new AddCommand( 2 ) );
  expect( ExecCalc.getCurrentValue() ).toBe(4);
})

test('Subtract', () => {
  ExecCalc.currentValue = 0;
  ExecCalc.execute( new SubCommand( 1 ) );
  expect( ExecCalc.getCurrentValue() ).toBe(-1);
})

test('Multiplying', () => {
  ExecCalc.currentValue = 10;
  ExecCalc.execute( new MultiplyCommand( 5 ) );
  expect( ExecCalc.getCurrentValue() ).toBe(50);
})

test('Dividing', () => {
  ExecCalc.currentValue = 15;
  ExecCalc.execute( new DivideCommand( 3 ) );
  expect( ExecCalc.getCurrentValue() ).toBe(5);
})


test('Undo', () => {
  let previousVal = ExecCalc.currentValue;

  ExecCalc.execute( new SubCommand( 5 ) );
  ExecCalc.undo();
  expect( ExecCalc.getCurrentValue() ).toBe(previousVal);

  ExecCalc.execute( new AddCommand( 10 ) );
  ExecCalc.undo();
  expect( ExecCalc.getCurrentValue() ).toBe(previousVal);

  ExecCalc.execute( new MultiplyCommand( 2 ) );
  ExecCalc.undo();
  expect( ExecCalc.getCurrentValue() ).toBe(previousVal);

  ExecCalc.execute( new DivideCommand( 6 ) );
  ExecCalc.undo();
  expect( ExecCalc.getCurrentValue() ).toBe(previousVal);
})

test('Some Calculations', () => {
  ExecCalc.currentValue = 0;
  ExecCalc.execute( new AddCommand( 100 ) );
  ExecCalc.execute( new SubCommand( 10 ) );
  ExecCalc.execute( new MultiplyCommand( 2 ) );
  ExecCalc.execute( new DivideCommand( 3 ) );
  expect( ExecCalc.getCurrentValue() ).toBe(60);
})
