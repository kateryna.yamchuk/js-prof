  class Calc {
    constructor(){
      this.currentValue = 0;
      this.history = [];
    }
    execute( command ){
      this.currentValue = command.do( this.currentValue );
      this.history.push(command);
    }

    undo(){
    const cmd = this.history.pop();
    this.currentValue = cmd.undo( this.currentValue );
    }

    getCurrentValue(){
      console.log('CurrentValue is:', this.currentValue);
      return( this.currentValue );
    }
  }

  function Command(fn, undo, value) {
      this.do = fn;
      this.undo = undo;
      this.value = value;
  }

 function add(value){
    return value + this.value;
  }

 function sub(value){
    return value - this.value;
  }

 function multiply(value){
  return value * this.value;
 }

 function divide(value){
  return value / this.value;
 }

export function AddCommand(value){
    Command.call( this, add, sub, value);
  }
export function SubCommand(value){
    Command.call( this, sub, add, value);
  }
export function MultiplyCommand(value){
  Command.call( this, multiply, divide, value);
}
export function DivideCommand(value){
  Command.call( this, divide, multiply, value);
}

let Calculator = new Calc();

export const execCalculator = () => {
  Calculator.execute( new AddCommand( 20 ) );
  Calculator.execute( new DivideCommand( 2 ) );
  Calculator.execute( new MultiplyCommand( 5 ) );
  Calculator.execute( new AddCommand( 30 ) );
  Calculator.execute( new SubCommand( 10 ) );

  console.log("after last subtraction", Calculator.getCurrentValue());
  Calculator.undo();
  console.log("after undo", Calculator.getCurrentValue());

  Calculator.execute( new AddCommand( 3 ) );
  Calculator.execute( new AddCommand( 16 ) );
  Calculator.undo();
  Calculator.execute( new SubCommand( 10 ) );
  Calculator.getCurrentValue();
}

export default Calc;
