/*

  Задание - используя классы и (или) прототипы создать программу, которая будет
  распределять животных по зоопарку.

  Zoo ={
    name: '',
    AnimalCount: 152,
    zones: {
      mammals: [],
      birds: [],
      fishes: [],
      reptile: [],
      others: []
    },
    addAnimal: function(animalObj){
      // Добавляет животное в зоопарк в нужную зону.
      // зона берется из класса который наследует Animal
      // если у животного нету свойства zone - то добавлять в others
    },
    removeAnimal: function('animalName'){
      // удаляет животное из зоопарка
      // поиск по имени
    },
    getAnimal: function(type, value){
      // выводит информацию о животном
      // поиск по имени или типу где type = name/type
      // а value значение выбраного типа поиска
    },
    countAnimals: function(){
      // функция считает кол-во всех животных во всех зонах
      // и выводит в консоль результат
    }
  }

  Есть родительский класс Animal у которого есть методы и свойства:
  Animal {
    name: 'Rex', // имя животного для поиска
    phrase: 'woof!',
    foodType: 'herbivore' | 'carnivore', // Травоядное или Плотоядное животное
    eatSomething: function(){ ... }
  }

  Дальше будут классы, которые расширяют класс Animal - это классы:
  - mammals
  - birds
  - fishes
  - pertile

  каждый из них имеет свои свойства и методы:
  в данном примере уникальными будут run/speed. У птиц будут методы fly & speed и т.д
  Mammals = {
    zone: mamal, // дублирует название зоны, ставиться по умолчанию
    type: 'wolf', // что за животное
    run: function(){
      console.log( wolf Rex run with speed 15 km/h );
    },
    speed: 15
  }

  Тестирование:
    new Zoo('name');

    var Rex = new Mammal('Rex', 'woof', 'herbivore', 15 );

    your_zooName.addAnimal(Rex);

      // Добавит в your_zooName.zones.mamals новое животное.
    var Dex = new Mammal('Dex', 'woof', 'herbivore', 11 );
    your_zooName.addAnimal(Dex);
      // Добавит в your_zooName.zones.mamals еще одно новое животное.

    your_zooName.get('name', 'Rex'); -> {name:"Rex", type: 'wolf' ...}
    your_zooName.get('type', 'wolf'); -> [{RexObj},{DexObj}];

    Программу можно расширить и сделать в виде мини игры с интерфейсом и сдать
    как курсовую работу!
    Идеи:
    - Добавить ранжирование на травоядных и хищников
    - добавив какую-то функцию которая иммитирует жизнь в зоопарке. Питание, движение, сон животных и т.д
    - Условия: Если хищник и травоядный попадает в одну зону, то хищник сьедает травоядное и оно удаляется из зоопарка.
    - Графическая оболочка под программу.
*/

class Zoo {
  constructor (name){
    this.name = name;
    //this.AnimalCount = 0;
    this.zone = {
      mammals : [],
      birds : [],
      fishes : [],
      reptiles : [],
      others : [],
    }
  }

  addAnimal(animalObj) {
    if (animalObj.zone in this.zone){
      this.zone[animalObj.zone].push(animalObj)
    }else{
      this.zone["others"].push(animalObj)
    }
  }

  removeAnimal(animalName){
    let searchAnimal;
    for (let key in this.zone){
      this.zone[key].forEach( (item) => {
        if (item.name == animalName){
          this.zone[key].splice(this.zone[key].indexOf(item)-2,1);
          return;
        }
      })
    }
  }

  getAnimal(type,value){
    let animalsArray = [];
    for (let key in this.zone){
       let animal = this.zone[key].filter( (item) => {
          return item[type] == value
      })
       if (animal.length > 0){
        Array.prototype.push.apply(animalsArray, animal);
       }
       
    }
    console.log(animalsArray);
  }

  countAnimals (){
      let count = 0;
      for (let key in this.zone){
        count = count + this.zone[key].length;
      }
      console.log(`В зоопарке ${count} животных`);
    }
}
  

class Animal {
  constructor (name,phrase,foodType){
    this.name = name;
    this.phrase = phrase;
    this.foodType = foodType;
  }
}

class Mammals extends Animal{
  constructor(name,phrase,foodType,speed,type){
    super(name,phrase,foodType);
      this.speed = speed;
      this.zone = 'mammals';
      this.type = type;
  }
  
  run(){
    console.log( `${this.type} ${this.name} runs with speed ${this.speed} km/h` )
  }
}

class Birds extends Animal{
  constructor(name,phrase,foodType,speed,type){
    super(name,phrase,foodType);
      this.speed = speed;
      this.zone = 'birds';
      this.type = type;
  }
  
  fly(){
    console.log( `${this.type} ${this.name} flies with speed ${this.speed} km/h` )
  }
}

class Fishes extends Animal{
  constructor(name,phrase,foodType,speed,type){
    super(name,phrase,foodType);
      this.speed = speed;
      this.zone = 'fishes';
      this.type = type;
  }
  swim(){
    console.log( `${this.type} ${this.name} swims with speed ${this.speed} km/h` )
  }
  
}

class Reptiles extends Animal{
  constructor(name,phrase,foodType,speed,type){
    super(name,phrase,foodType);
      this.speed = speed;
      this.zone = 'reptiles';
      this.type = type;
  }

  run(){
    console.log( `${this.type} ${this.name} runs with speed ${this.speed} km/h` )
  }
}

let myZoo = new Zoo('Киевский зоопарк');
let Rex = new Mammals('Rex', 'woof', 'carnivore', 15, 'wolf');
let John = new Mammals('John', '', 'herbivore', 6, 'giraffe');
let Dex = new Birds('Dex', 'cock-a-doodle-doo', 'herbivore', 5, 'cock' );
let Pet = new Reptiles('Pet', 'hr-hr-hr', 'carnivore', 4, 'crocodile' );
let Ron = new Fishes('Ron', '', 'herbivore', 10, 'clownfish' );

myZoo.addAnimal(Rex);
myZoo.addAnimal(Dex);
myZoo.addAnimal(John);
myZoo.addAnimal(Pet);
myZoo.addAnimal(Ron);

console.log('With all animals ',myZoo);
console.log('Before deletion');
myZoo.countAnimals();

console.log('Search by type:');
myZoo.getAnimal('type','wolf');
myZoo.getAnimal('speed',5);
myZoo.getAnimal('foodType','carnivore');

myZoo.removeAnimal('Rex');
myZoo.removeAnimal('Dex');

console.log("My Zoo after deletion", myZoo);
myZoo.countAnimals();

