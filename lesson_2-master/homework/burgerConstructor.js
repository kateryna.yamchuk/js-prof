

  /*

    Задание:

      1. Создать конструктор бургеров на прототипах, которая добавляет наш бургер в массив меню

      Дожно выглядеть так:
      new burger('Hamburger',[ ...Массив с ингредиентами ] , 20);

      Моделька для бургера:
      {
        cookingTime: 0,     // Время на готовку
        showComposition: function(){
          let {composition, name} = this;
          let compositionLength = composition.length;
          console.log(compositionLength);
          if( compositionLength !== 0){
            composition.map( function( item ){
                console.log( 'Состав бургера', name, item );
            })
          }
        }
      }

      Результатом конструктора нужно вывести массив меню c добавленными элементами.
      // menu: [ {name: "", composition: [], cookingTime:""},  {name: "", composition: [], cookingTime:""}]

        2. Создать конструктор заказов

        Моделька:
        {
          id: "",
          orderNumber: "",
          orderBurder: "",
          orderException: "",
          orderAvailability: ""
        }

          Заказ может быть 3х типов:
            1. В котором указано название бургера
              new Order('Hamburger'); -> Order 1. Бургер Hamburger, будет готов через 10 минут.
            2. В котором указано что должно быть в бургере, ищете в массиве Menu подходящий вариант
              new Order('', 'has', 'Название ингредиента') -> Order 2. Бургер Чизбургер, с сыром, будет готов через 5 минут.
            3. В котором указано чего не должно быть
              new Order('', 'except', 'Название ингредиента') ->


            Каждый их которых должен вернуть статус:
            "Заказ 1: Бургер ${Название}, будет готов через ${Время}

            Если бургера по условиям заказа не найдено предлагать случайный бургер из меню:
              new Order('', 'except', 'Булка') -> К сожалению, такого бургера у нас нет, можете попробовать "Чизбургер"
              Можно например спрашивать через Confirm подходит ли такой вариант, если да - оформлять заказ
              Если нет, предложить еще вариант из меню.

        3. Протестировать программу.
          1. Вначале добавляем наши бургеры в меню (3-4 шт);
          2. Проверяем работу прототипного наследования вызывая метод showComposition на обьект бургера с меню
          3. Формируем 3 заказа

        Бонусные задания:
        4. Добавлять в исключения\пожелания можно несколько ингредиентов
        5. MEGABONUS
          Сделать графическую оболочку для программы.

  */

  const Ingredients = [
    'Булка',
    'Огурчик',
    'Котлетка',
    'Бекон',
    'Рыбная котлета',
    'Соус карри',
    'Кисло-сладкий соус',
    'Помидорка',
    'Маслины',
    'Острый перец',
    'Капуста',
    'Кунжут',
    'Сыр Чеддер',
    'Сыр Виолла',
    'Сыр Гауда',
    'Майонез'
  ];

  var OurMenu = [];
  var OurOrders = [];
  let id = 1;
  function Burger( name, ingredients, cookingTime){
    this.name = name;
    this.ingredients = ingredients;
    this.cookingTime = cookingTime;
    OurMenu.push(this);
  }

  const F = (Obj) => {
      let auxBurgers = Object.assign([], OurMenu); //клон содержаемых обьектов в массиве

         burger = auxBurgers[Math.floor(Math.random()*auxBurgers.length)];

         let result = confirm(`К сожалению, такого бургера у нас нет, можете попробовать ${burger.name}`);
         
         auxBurgers.splice(auxBurgers.indexOf(burger),1);

         if (result){
            return `Заказ ${Obj.orderNumber}. Бургер ${burger.name} будет готов через ${burger.cookingTime} минут.`
         } else {
            while (!result) {
              burger = auxBurgers[Math.floor(Math.random()*auxBurgers.length)];
              result = confirm(`Можете так же попробовать ${burger.name}`);
              auxBurgers.splice(auxBurgers.indexOf(burger),1);
              if (auxBurgers.length < 1){
                result = true;
                return `Извнините, но вам не угодить!`;
              }
            }
            return `Заказ ${Obj.orderNumber}. Бургер ${burger.name} будет готов через ${burger.cookingTime} минут.`
         }
  }


  Burger.prototype.showComposition =  function(){
    let {name, ingredients} = this;
    let ingredientsLength = ingredients.length;
    //console.log(ingredientsLength);
    if( ingredientsLength !== 0){
      console.log( 'Состав бургера :', name)
      ingredients.map( function( item ){
      console.log( "- ",item );
      })
    }
  }

  Burger.prototype.getProperties = function(){
    let {name, ingredients, cookingTime} = this;
    return {"name" : name, "ingredients" : ingredients, "cookingTime":cookingTime};
  }

new Burger ('Hamburger',['Булка','Кунжут','Котлетка','Сыр Чеддер','Майонез'] , 20);
new Burger ('DoubleCheeze',['Булка','Огурчик','Котлетка','Сыр Чеддер','Соус карри'] , 30);
new Burger ('Big Tasty',['Булка','Кунжут','Котлетка','Сыр Чеддер','Майонез', 'Кунжут','Помидорка'] , 40);
new Burger ('Filet o fish',['Булка','Рыбная котлета','Сыр Чеддер','Майонез'] , 25);

console.log("OurMenu", OurMenu);
OurMenu.forEach( (item) => {
  item.showComposition()
})

  function Order(name, condition, value){
    this.name = name;
    this.condition = condition;
    this.value = value;
    this.orderNumber = id;

    OurOrders.push(this);
    console.log(this.findBurger());
}

  Order.prototype.findBurger = function () {
    id++;
    if (this.name != ""){
      let burger = OurMenu.find( (item) => {
        return (item.name == this.name);
      })
  
      return `Заказ ${this.orderNumber}. Бургер ${this.name} будет готов через ${burger.cookingTime} минут.`
    }
    if (this.condition == "has"){
      let burger = OurMenu.find( (item) => {
        return item.ingredients.find( (item) => {
          return (item == this.value)
        });
      })
      if (burger){
        return `Заказ ${this.orderNumber}. Бургер ${burger.name} с ингридиентом ${this.value} будет готов через ${burger.cookingTime} минут.`
      }else{ 
          return F(this);         
      }
    }

    if (this.condition == "except"){
      let burger = OurMenu.find ( (item) => {
          let flag = item.ingredients.some( (item) => {
          return (item !== this.value)
          });
      return (flag == true)
      })
      return `Заказ ${this.orderNumber}. Бургер ${burger.name} без ингридиента ${this.value} будет готов через ${burger.cookingTime} минут.`
    }
  }

  new Order ('Hamburger');
  new Order ('DoubleCheeze');
  new Order ('','has','Рыбная котлета');
  new Order ('','has','Кетчуп');
  new Order ('','except','Кунжут');
