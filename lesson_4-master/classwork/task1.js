/*
  Composition:

  Задание при помощи композиции создать объекты 4х типов:

  functions:
    - MakeBackendMagic
    - MakeFrontendMagic
    - MakeItLooksBeautiful
    - DistributeTasks
    - DrinkSomeTea
    - WatchYoutube
    - Procrastinate

  BackendDeveloper = MakeBackendMagic + DrinkSomeTea + Procrastinate
  FrontendDeveloper = MakeFrontendMagic + DrinkSomeTea + WatchYoutube
  Designer = MakeItLooksBeautiful + WatchYoutube + Procrastinate
  ProjectManager = DistributeTasks + Procrastinate + DrinkSomeTea

  ProjectManager(name,gender,age) => { name, gender, age, type: 'project'}

*/



  const MakeBackendMagic = ( person ) => ({
    makebackendmagic: () => console.log('MakeBackendMagic wowowowo' )
  });

  const DrinkSomeTea = ( person ) => ({
    drinkSomeTea: () => console.log('DrinkSomeTea wowowowo' )
  });

  const Procrastinate = ( person ) => ({
    procrastinate: () => console.log('Procrastinate wowowowo' )
  });

  const MakeFrontendMagic = ( person ) => ({
    makeFrontendMagic: () => console.log('MakeFrontendMagic wowowowo' )
  });

  const WatchYoutube = ( person ) => ({
    watchYoutube: () => console.log('WatchYoutube wowowowo' )
  });

  const MakeItLooksBeautiful = ( person ) => ({
    makeItLooksBeautiful: () => console.log('MakeItLooksBeautiful wowowowo' )
  });

  const DistributeTasks = ( person ) => ({
    distributeTasks: () => console.log('DistributeTasks wowowowo' )
  });


  export const BackendDeveloper = (name, gender, age) => {
    let person = {
      name,
      gender,
      age,
      type : "backend"
    };

    return Object.assign(
      {},
      person,
      MakeBackendMagic(person),
      DrinkSomeTea(person),
      Procrastinate(person)
    );
  };

  export const FrontendDeveloper = (name, gender, age) => {
    let person = {
      name,
      gender,
      age,
      type : "frontend"
    };

    return Object.assign(
      {},
      person,
      MakeFrontendMagic(person),
      DrinkSomeTea(person),
      WatchYoutube(person)
    );
  };

  export const Designer = (name, gender, age) => {
    let person = {
      name,
      gender,
      age,
      type : "design"
    };

    return Object.assign(
      {},
      person,
      Procrastinate(person),
      MakeItLooksBeautiful(person),
      WatchYoutube(person)
    );
  };

  export const ProjectManager = (name, gender, age) => {
    let person = {
      name,
      gender,
      age,
      type : "project"
    };

    return Object.assign(
      {},
      person,
      Procrastinate(person),
      DrinkSomeTea(person),
      DistributeTasks(person)
    );
  };


  




