/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _classworks_mediator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../classworks/mediator */ \"./classworks/mediator.js\");\n\nObject(_classworks_mediator__WEBPACK_IMPORTED_MODULE_0__[\"default\"])();\n\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./classworks/mediator.js":
/*!********************************!*\
  !*** ./classworks/mediator.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/*\n  Написать медиатор для группы студентов.\n\n  Профессор отвечает только на вопросы старосты.\n\n  У Студента есть имя и группа которой он пренадлежит.\n  Он может запросить старосту задать вопрос или получить ответ.\n\n  Староста может добавлять студентов в группу и передавать вопрос профессору.\n*/\n\nconst Mediator = () => {\n\n  console.log( 'your code ');\n\n  class Professor {\n    answerTheQuestion( student, question ){\n      if( student.type !== 'monitor'){\n        console.error('It\\'s not your bussines');\n      } else {\n        console.log('Yes, my dear?!');\n      }\n    }\n  }\n\n   class Student {\n    constructor(name){\n      this.name = name;\n      this.type = 'student';\n      this.mentor = null;\n    }\n    getAnswer(from, question){\n      from.answerTheQuestion(this, question);\n    }\n\n    tipTheMonitor(message, prof){\n     if (this.mentor === null){\n      console.error('You can not tip the mentor');\n     }\n     this.mentor.askProfessor(message ,this, prof);\n    }\n  }\n\n  // Monitor == Староста\n  class Monitor extends Student{\n    constructor(name){\n      super(name);\n      this.studentsGroup = {};\n      this.type = 'monitor';\n    }\n    \n    addToGroup(student){\n      this.studentsGroup[student.name] = student;\n      student.mentor = this;\n\n    }\n\n    askProfessor(message, from, to){\n      this.getAnswer(to , message);\n    }\n  }\n\n  let MyProfessor = new Professor();\n  let MyMonitor = new Monitor(\"Masha Petrova\");\n  let student = new Student(\"Petya Ivanov\");\n  \n  MyMonitor.getAnswer(MyProfessor, 'bla bla bla?');\n  student.getAnswer(MyProfessor, 'bla bla bla?');\n\n  MyMonitor.addToGroup(student);\n  student.tipTheMonitor('bla bla bla?',MyProfessor);\n\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Mediator);\n\n\n//# sourceURL=webpack:///./classworks/mediator.js?");

/***/ })

/******/ });