/*
  Написать медиатор для группы студентов.

  Профессор отвечает только на вопросы старосты.

  У Студента есть имя и группа которой он пренадлежит.
  Он может запросить старосту задать вопрос или получить ответ.

  Староста может добавлять студентов в группу и передавать вопрос профессору.
*/

const Mediator = () => {

  console.log( 'your code ');

  class Professor {
    answerTheQuestion( student, question ){
      if( student.type !== 'monitor'){
        console.error('It\'s not your bussines');
      } else {
        console.log('Yes, my dear?!');
      }
    }
  }

   class Student {
    constructor(name){
      this.name = name;
      this.type = 'student';
      this.mentor = null;
    }
    getAnswer(from, question){
      from.answerTheQuestion(this, question);
    }

    tipTheMonitor(message, prof){
     if (this.mentor === null){
      console.error('You can not tip the mentor');
     }
     this.mentor.askProfessor(message ,this, prof);
    }
  }

  // Monitor == Староста
  class Monitor extends Student{
    constructor(name){
      super(name);
      this.studentsGroup = {};
      this.type = 'monitor';
    }
    
    addToGroup(student){
      this.studentsGroup[student.name] = student;
      student.mentor = this;

    }

    askProfessor(message, from, to){
      this.getAnswer(to , message);
    }
  }

  let MyProfessor = new Professor();
  let MyMonitor = new Monitor("Masha Petrova");
  let student = new Student("Petya Ivanov");
  
  MyMonitor.getAnswer(MyProfessor, 'bla bla bla?');
  student.getAnswer(MyProfessor, 'bla bla bla?');

  MyMonitor.addToGroup(student);
  student.tipTheMonitor('bla bla bla?',MyProfessor);

}

export default Mediator;
